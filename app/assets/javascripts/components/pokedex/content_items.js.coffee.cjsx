{Col, Row } = ReactBootstrap

PokemonContentItems = React.createClass
  componentDidMount: ->

  componentWillUnMount: ->

  render: ->
    <div className="container">
      <Row>
        <Col md={3}>
          <p className="lead">Filter</p>
          <div className="list-group">
            <a className="list-group-item" href="#">Category 1</a>
            <a className="list-group-item" href="#">Category 2</a>
            <a className="list-group-item" href="#">Category 3</a>
          </div>
        </Col>
        <Col md={9}>
          <Row>
            <Col sm={2} lg={2} md={2}>
              <div className="thumbnail" style={padding: 0, boxShadow: '2px 2px 3px 4px #EBEBEB'}>
                <img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/4.png"/>
                <div className="caption">
                  <h4 style={fontSize: '16px',textAlign: 'center',width: '100%'}><a href="#">First Product</a></h4>
                </div>
              </div>
            </Col>

            <Col sm={2} lg={2} md={2}>
              <div className="thumbnail" style={padding: 0, boxShadow: '2px 2px 3px 4px #EBEBEB'}>
                <img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/3.png"/>
                <div className="caption">
                  <h4 style={fontSize: '16px',textAlign: 'center',width: '100%'}><a href="#">First Product</a></h4>
                </div>
              </div>
            </Col>

            <Col sm={2} lg={2} md={2}>
              <div className="thumbnail" style={padding: 0, boxShadow: '2px 2px 3px 4px #EBEBEB'}>
                <img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"/>
                <div className="caption">
                  <h4 style={fontSize: '16px',textAlign: 'center',width: '100%'}><a href="#">First Product</a></h4>
                </div>
              </div>
            </Col>

            <Col sm={2} lg={2} md={2}>
              <div className="thumbnail" style={padding: 0, boxShadow: '2px 2px 3px 4px #EBEBEB'}>
                <img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/8.png"/>
                <div className="caption">
                  <h4 style={fontSize: '16px',textAlign: 'center',width: '100%'}><a href="#">First Product</a></h4>
                </div>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
window.PokemonContentItems = PokemonContentItems
