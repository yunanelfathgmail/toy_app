{EventEmitter} = fbemitter

CHANGE_EVENT = 'change'
ITEM_CHANGE_EVENT = 'change:item'

window.PokedexStore = _.assign(new EventEmitter(), {
  items: []

  requesting: { type: null, status: false }

  getFilterByKey: (array, key) ->
    _.find(array, (e) -> e.key == key)

  emitChange: -> @emit(CHANGE_EVENT)
  addChangeListener: (callback) -> @addListener(CHANGE_EVENT, callback)

  emitItemChange: -> @emit(ITEM_CHANGE_EVENT)
  addItemChangeListener: (callback) -> @addListener(ITEM_CHANGE_EVENT, callback)
})

dispatcher.register (payload) ->
  switch payload.actionType
    when 'screen-set-items'
      items = PokedexStore.items
      if payload?.onReset
        PokedexStore.items = payload.items
      else
        if items.length > 0
          for item in payload.items
            if $.inArray(item.pub_id, _.map(items, (e) -> e.pub_id)) < 0
              items.push(item)
        else
          PokedexStore.items = payload.items

      PokedexStore.emitChange()
    when 'screen-filter-set-type'
      if payload.type == 'sizeLists'
        items = _.map(payload.items, (e) -> {name: "#{e} Inch", key: e})
      else if payload.type == 'cityLists'
        items = _.map(payload.items, (e) -> {name: e.name, key: e.id, id: e.id})
      else
        items = _.map(payload.items, (e, i) -> {name: i, key: e})
      PokedexStore[payload.type] = items
      PokedexStore.emitChange()
    when 'screen-filter-device-set-checked-filter'
      item = _.find(PokedexStore[payload.type], (e) -> e[payload.sourceKey] == payload.key)
      _.assign(item, payload.attributes)
      PokedexStore.emitItemChange(item)
      PokedexStore.emitChange()
    when 'adx-global-attributes-setter'
      _.assign(PokedexStore, payload.attributes)
      PokedexStore.emitChange()
